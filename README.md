# HomePi #

## About ##
This is an ongoing development for android device to SSH onto a remote server to trigger a bash command for 
USB relay.  The purpose of this project was to have a rasberryPi connected to a remote garage door through the 
GPIO pins. With SSH enable on the RPI, the HomePi will gather the username and password to connect and send the 
following request to the RPI.  
The application was develop for a garage door opener, but it the implementation was developed for additional 
use of relay switch and home automation.

## Current Features ##

  * Current the usename and pass is hardcoded to your device SSH, within the SSHConnect.java
  * Following libraries: JCraft -> jsch v0.1.49 & jzlib v1.1.1 <http://www.jcraft.com/>


## Documentation Generation ##
  
	1. This is still in development and eventually additional features will be implemented.
	* GPS location
	* Voice Command
	* Access Rights and Database support for multiple devices




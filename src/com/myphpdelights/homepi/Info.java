package com.myphpdelights.homepi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.widget.TextView;

public class Info extends Activity {

	TextView textMsg;
	final String textSource = "http://myphpdelights.com/development/android/homepi.txt";

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
				.detectDiskReads().detectDiskWrites().detectNetwork()
				.penaltyLog().build());

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info);
	

		textMsg = (TextView) findViewById(R.id.textmsg);

		URL textUrl;
		try {
			textUrl = new URL(textSource);
			BufferedReader bufferReader = new BufferedReader(
					new InputStreamReader(textUrl.openStream()));
			String StringBuffer;
			String stringText = "";
			while ((StringBuffer = bufferReader.readLine()) != null) {
				stringText += StringBuffer;
			}
			bufferReader.close();
			textMsg.setText(stringText);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_info, menu);
		return true;
	}

}

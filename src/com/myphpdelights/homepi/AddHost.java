package com.myphpdelights.homepi;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import static com.myphpdelights.database.Constants.*;
import com.myphpdelights.database.HostsData;

public class AddHost extends Activity {

	private HostsData hosts;
	private String port = "22";
	private String myHost;
	private String name;
	private String username;
	private String password;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_host);

	}

	public void addhost() {
		final EditText nameField = (EditText) findViewById(R.id.name);
		name = nameField.getText().toString();

		final EditText hostField = (EditText) findViewById(R.id.host);
		myHost = hostField.getText().toString();

		final EditText portField = (EditText) findViewById(R.id.port);
		String myPort = portField.getText().toString();

		if (myPort != null && myPort.length() > 0)
			port = myPort;

		final EditText unameField = (EditText) findViewById(R.id.uname);
		username = unameField.getText().toString();

		final EditText passField = (EditText) findViewById(R.id.pass);
		password = passField.getText().toString();

		hosts = new HostsData(this);

		try {
			addHosts(name, myHost, username, password, port);
			Cursor cursor = getHosts();
			showHosts(cursor);
		} finally {
			hosts.close();
		}

	}

	private void showHosts(Cursor cursor) {
		
		  StringBuilder builder = new StringBuilder();
		 
		 while(cursor.moveToNext()){
		 
		 long id = cursor.getLong(0); 
		 name = cursor.getString(1); 
		 String host = cursor.getString(2); 
		 String user = cursor.getString(3); String
		 pass = cursor.getString(4); 
		 long port = cursor.getLong(5);
		 
		 builder.append(id).append(": "); 
		 builder.append(name).append(": "); 
		 builder.append(host).append(": "); 
		 builder.append(user).append(": ");
		 builder.append(pass).append(": ");
		 builder.append(port).append(": \n");
		 
		 
		 }
		 
		 TextView text = (TextView) findViewById(R.id.hosts);
		 text.setText(builder);
		 

	}

	
	 private static String[] FROM = {_ID,INSTANCE,HOST,USER,PASS,PORT}; 
	 private static String ORDER_BY = _ID + " ASC";
	 
	 private Cursor getHosts() { SQLiteDatabase db =
	 hosts.getReadableDatabase(); Cursor cursor =
	 db.query(TABLE_NAME,FROM,null,null,null,null,ORDER_BY);
	 startManagingCursor(cursor); return cursor; }
	 
	 private void addHosts(String instance, String host, String username, String pass, String
	 port) {
	 
	 SQLiteDatabase db = hosts.getWritableDatabase(); ContentValues values =
	 new ContentValues(); 
	 values.put(HOST,host);
	 values.put(INSTANCE,instance);
	 values.put(USER,username); 
	 values.put(PASS,pass); 
	 values.put(PORT,port);
	 
	 db.insertOrThrow(TABLE_NAME,null,values);
	 
	 }
	 

}

package com.myphpdelights.homepi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity implements Runnable {

	protected ProgressDialog pd;
	protected Vibrator v;
	private Thread thread;
	private SSHConnect ssh;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
				.detectDiskReads().detectDiskWrites().detectNetwork()
				.penaltyLog().build());

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		ssh = new SSHConnect();

		init();

	}

	public void init() {

		if (haveInternet()) {

			pd = ProgressDialog.show(this, null, "Loading... ", true, false);
			v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

			thread = new Thread(this);
			thread.start();

		} else {

			Toast.makeText(this, "No Network Detected", Toast.LENGTH_SHORT)
					.show();

		}

	}

	public void sendButton(View view) {
		if (haveInternet()) {
			try {

				ssh.sendCommand();
				// Vibrate for 300 milliseconds
				v.vibrate(300);

			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(this, "Failed to send command",
						Toast.LENGTH_SHORT).show();

			}
		} else {

			Toast.makeText(this, "No Network Detected", Toast.LENGTH_SHORT)
					.show();

		}

	}

	/**
	 * Checks if we have a valid Internet Connection on the device.
	 * 
	 * @param ctx
	 * @return True if device has internet
	 * 
	 */
	public boolean haveInternet() {
		boolean haveInternet = false;

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni != null) {
			if (ni.getType() == ConnectivityManager.TYPE_WIFI) {
				if (ni.isConnectedOrConnecting())
					haveInternet = true;

			}
			if (ni.getType() == ConnectivityManager.TYPE_MOBILE) {
				if (ni.isConnectedOrConnecting())
					haveInternet = true;

			}
		}

		return haveInternet;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_info:
			startActivity(new Intent(this, Info.class));
			return true;

		case R.id.menu_preferences:
			startActivity(new Intent(this, Settings.class));
			// Toast.makeText(this,"Preferences is Selected",Toast.LENGTH_SHORT).show();
			return true;

		case R.id.menu_exit:
			exit();
			return true;
		case R.id.menu_rescan:
			init();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	public void run() {

		if (ssh.connect()) {
			v.vibrate(300);
		}
		pd.dismiss();

	}

	public void onDestroy() {
		SSHConnect.disconnect();
	}

	public void exit() {

		SSHConnect.disconnect();

		this.finish();
		System.exit(0);

	}

}

package com.myphpdelights.homepi;

import java.io.ByteArrayOutputStream;
import java.util.Properties;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class SSHConnect {

	static Session session;
	static Channel channel;

	static String username = "username";
	static String password = "password";
	static String hostname = "hostname";
	static String command = "batch/./relay.sh";
	static int port = 22;

	public void setCommand(String command) {

		SSHConnect.command = command;

	}



	/**
	 * SSHConnect to Host return true if connected
	 * 
	 * @return boolean
	 */
	public boolean connect() {


			// Avoid asking for key confirmation
			Properties prop = new Properties();
			prop.put("StrictHostKeyChecking", "no");
			prop.put("compression.s2c", "zlib,none");
			prop.put("compression.c2s", "zlib,none");

			JSch jsch = new JSch();

			try {
				session = jsch.getSession(username, hostname, port);
				session.setConfig(prop);
				session.setPassword(password);

				session.connect(3000);
				if (session.isConnected()) {
					return true;

				} else {

					return false;
				}
			} catch (JSchException e1) {
				e1.printStackTrace();
			}
		

		return false;

	}

	public boolean isConnect() {

		return session.isConnected();
	}

	public void sendCommand() throws JSchException {

		// SSH Channel
		channel = session.openChannel("exec");

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		channel.setOutputStream(baos);

		// Execute command
		((ChannelExec) channel).setCommand(command);
		channel.setInputStream(null);

		channel.connect();

		channel.disconnect();

	}

	public static void disconnect() {

		session.disconnect();

	}
}

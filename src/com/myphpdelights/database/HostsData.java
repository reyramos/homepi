package com.myphpdelights.database;

import static com.myphpdelights.database.Constants.*;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class HostsData extends SQLiteOpenHelper {

	public static final String DBNAME = "hosts.db";
	public static final int DBVRSN = 1;

	public HostsData(Context context) {
		super(context, DBNAME, null, DBVRSN);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE "+TABLE_NAME+" ("+_ID +
				" INTEGER PRIMARY KEY AUTOINCREMENT, "+
				ACT + " INTERGER NULL, " +
				INSTANCE + " TEXT NOT NULL, " +
				HOST + " TEXT NOT NULL, " +
				USER + " TEXT NOT NULL, " +
				PASS + " TEXT NOT NULL, " +
				PORT + " INTERGER NOT NULL);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
		onCreate(db);
		
	}
	
	
}

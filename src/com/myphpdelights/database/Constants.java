package com.myphpdelights.database;

import android.provider.*;

public interface Constants extends BaseColumns {

	public static final String TABLE_NAME = "hosts";
	
	public static final String ACT = "act";
	public static final String INSTANCE = "instance";
	public static final String HOST = "host";
	public static final String USER = "user";
	public static final String PASS = "pass";
	public static final String PORT = "port";

}
